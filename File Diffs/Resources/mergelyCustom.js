
var comp = $('#compare');

function downloadJSON(url, callback) {
  $.get(url, function(data) {
    var json = JSON.parse(data);
    var formattedText = JSON.stringify(json, null, 2);
    callback(formattedText);
  });
}

comp.mergely({
  cmsettings: {
    readOnly: false,
    lineWrapping: true
  },
  wrap_lines: true,

  //Doesn't do anything?
  //autoresize: true,

  editor_width: 'calc(50% - 25px)',
  editor_height: '100%',

  lhs: function(setValue) {
    downloadJSON("https://gist.githubusercontent.com/quotient/c426a69bfa550d1a43a0/raw/645d996d8d23e48dc9b691a10238d690df845f4f/gistfile1.txt", setValue);
  },
  rhs: function(setValue) {
    downloadJSON("https://gist.githubusercontent.com/quotient/a9baf2c58972aa22c621/raw/c8f6caf22d330cb34fc6f70ece44699312b2b7b4/gistfile1.txt", setValue);
  }
});

function changeOptions(changer) {
  var options = comp.mergely('options');
  changer(options);

  comp.mergely('options', options);
  //comp.mergely('update');
}

$('#prev').click(function() { comp.mergely('scrollToDiff', 'prev'); });
$('#next').click(function() { comp.mergely('scrollToDiff', 'next'); });
$('#wrap').click(function() { changeOptions(function(x) { x.wrap_lines = !x.wrap_lines; }); });
