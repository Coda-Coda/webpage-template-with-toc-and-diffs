/*
A dynamic table of contents generator, by Daniel Britten.
Feel free to modify this file to suit your needs.
Inspired by Matt Whitlock's toc generator available at http://www.whitsoftdev.com/articles/toc.html
*/

function toArray(nodeList) {
    acc = [];
    nodeList.forEach(element => {
        acc = acc.concat([element]);
    });
    return acc;
}

function repeatReplace(s, oldChar, newChar) {
    for (let index = 0; index < s.length; index++) {
        s = s.replace(oldChar, newChar);
    }
    return s;
}

function processIndividualTOCEntry(tocCurrentElement, currentElement, sectionNameWithDots, sectionName) {
    currentElement.innerHTML = sectionNameWithDots + " " + currentElement.innerHTML
    currentElement.id = "section" + sectionNameWithDots
    createChildListItem(tocCurrentElement, "<a href=\"#section" + sectionNameWithDots + "\">" + sectionNameWithDots + " " + sectionName + "</a>");
}

function createChildListItem(currentNode, innerHTML) {
    child = document.createElement('li');
    child.innerHTML = innerHTML;
    currentNode.appendChild(child);
}

function changeDepth(headingLevel, previousHeadingLevel, tocElement) {
    newtocElement = tocElement;
    if (headingLevel < previousHeadingLevel) {
        for (let index = 0; index < (previousHeadingLevel - headingLevel); index++) {
            newtocElement = newtocElement.parentNode;
        }
        return newtocElement;
    }
    if (headingLevel > previousHeadingLevel) {
        for (let index = 0; index < (headingLevel - previousHeadingLevel); index++) {
            newtocElement = newtocElement.appendChild(document.createElement('ul'));
        }
        return newtocElement
    }
    //else (heading = previousHeadingLevel)
    return tocElement

}

function generateTOC(idToSearchInside, tocElement) {

    document.getElementById('title_in_toc').innerHTML = "<a href='#title'><u>" + document.getElementById('title').innerHTML + "</u></a>";
    document.getElementById('version_in_toc').innerHTML = document.getElementById('version').innerHTML;



    counter1 = 0;
    counter2 = 0;
    counter3 = 0;
    counter4 = 0;
    previousHeadingLevel = 1;

    tocElement = tocElement.appendChild(document.createElement('ul'));
    
    toArray(document.getElementById(idToSearchInside).childNodes).filter(function(x) {
        return (x.nodeName == 'H1' || x.nodeName == 'H2' || x.nodeName == 'H3' || x.nodeName == 'H4')
    }).forEach(element => {
        sectionNameWithDots = ""
        switch (element.nodeName) {
            case 'H1':
                tocElement = changeDepth(1, previousHeadingLevel, tocElement);
                previousHeadingLevel = 1
                counter1 += 1;
                counter2 = 0;
                counter3 = 0;
                counter4 = 0;
                sectionNameWithDots = counter1 + "."
                break;
        
            case 'H2':
                tocElement = changeDepth(2, previousHeadingLevel, tocElement);
                previousHeadingLevel = 2    
                counter2 += 1;
                counter3 = 0;
                counter4 = 0;
                sectionNameWithDots = counter1 + "." + counter2 + "."
                break;
            
            case 'H3':
                tocElement = changeDepth(3, previousHeadingLevel, tocElement);
                previousHeadingLevel = 3    
                counter3 += 1;
                counter4 = 0;
                sectionNameWithDots = counter1 + "." + counter2 + "." + counter3 + "."
                break;

            case 'H4':
                tocElement = changeDepth(4, previousHeadingLevel, tocElement);
                previousHeadingLevel = 4    
                counter4 += 1;
                sectionNameWithDots = counter1 + "." + counter2 + "." + counter3 + "." + counter4 + "."
                break;
            
            default:
                break;
        }
        processIndividualTOCEntry(tocElement, element, sectionNameWithDots, element.innerHTML);
    });
}